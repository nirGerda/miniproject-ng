To enable the extension to chrome browser follow these instructions:

1. Extract AntiPhishing_Nir&Almog to some folder of your choise.
2. Open chrome and and enter: "chrome://extensions/" inside url search box.
3. Press on "LOAD UNPACKED" on the top tool bar.
4. Navigate to the folder you extracted the project to, and select the folder "AntiPhishing_Nir&Almog".

Congratulations you just installed our chrome extension. ENJOY!

* To turn off / remove the extension - enter to chrome extensions page and slide the blue button / press on REMOVE.

All rights reserved to Nir Gerda and Almog Talker ©